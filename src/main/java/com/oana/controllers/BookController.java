package com.oana.controllers;

import com.oana.entities.Book;
import com.oana.entities.dao.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

@Controller
public class BookController {

    private BookRepository repo;

    @Autowired
    public BookController(BookRepository repo) {
        this.repo = repo;
    }
    
    @GetMapping("/api")
    public String getBooks(Model model) {
        model.addAttribute("books", repo.findAll());
        model.addAttribute("book", new Book());
        return "books";
    }
    
    @PostMapping("/api")
    @Transactional
    public String addBook(@ModelAttribute Book book) {
        repo.save(book);
        return "redirect:/api";
    }
    
    @GetMapping("/api/{isbn}")
    public String selectBook(@PathVariable("isbn") Long isbn, HttpSession session) {
        session.setAttribute("current", repo.findOne(isbn));
        return "redirect:/api";
    }
    
    @DeleteMapping("/api/{isbn}")
    @Transactional
    public String deleteBook(@PathVariable("isbn") Long isbn) {
        repo.delete(isbn);
        return "redirect:/api";
    }
    
    @PostMapping("/api/{isbn}/save")
    @Transactional
    public String saveBook(@PathVariable("isbn") Long isbn, @ModelAttribute Book book, HttpSession session) {
        Book b = repo.findOne(isbn);
        b.setTitle(book.getTitle());
        b.setAuthor(book.getAuthor());
        b.setPrice(book.getPrice());
        b.setGendre(book.getGendre());
        session.invalidate();
        return "redirect:/api";
    }

}
