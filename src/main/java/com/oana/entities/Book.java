package com.oana.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {

    @Id @GeneratedValue(strategy = GenerationType.AUTO) 
    private Long isbn;
    private String title;
    private String author;
    private Double price;
    private String gendre;

    public Book() {

    }

    public Book(String title, String author, Double price, String gendre) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.gendre = gendre;
    }

    public Long getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Double getPrice() {
        return price;
    }

    public String getGendre() {
        return gendre;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setGendre(String gendre) {
        this.gendre = gendre;
    }
}
